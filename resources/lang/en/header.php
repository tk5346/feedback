<?php
return [
    'home'=>'Home',
    'createFeedback'=>'Create Feedback',
    'login'=>'Login',
    'allFeedbacks'=>'All Feedbacks',
    'Create FeedBack'=>'Create Feedback',
    'Name'=>'Name',
    'Email'=>'Email',
    'Phone'=>'Phone',
    'RateService'=>'Rate Service',
    'RateFood'=>'Rate Food',
    'RateSanitation'=>'Rate Sanitation',
    'RateMusic'=>'Rate Music',
    'Body'=>'Write any Additional information you would like',
    'averages'=>'Averages',
    'TokenName'=>'Token String',

];
?>